# Promtail : collecte et envoie des logs à Loki

Ce service promtail doit être deployé sur toutes les machines virtuelles de production. Il récupère les logs du journal systemd et les logs des différents conteneurs Docker et les envoie sur le service Loki de la machine de monitoring.

Au premier lancement il faut créer le fichier de secrets à l'aide du fichier `secrets/promtail.secrets.example` et ajouter les bonnes valeurs. Le nom d'utilisateur et le mot de passe sont dans le Vaultwarden et la variable `HOSTNAME` doit être égale au nom de la machine sur laquelle le service tourne.
